//
//  RFModalConstants.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import UIKit

public enum RFModalConstants {
    public enum Node {
        static let options = [RFModalOptions.swipe, RFModalOptions.tapOutside]

        static let dismissTranslation = CGFloat(200)

        static let cornerRadius = CGFloat(10)

        enum Indicator {
            static let tintColor = UIColor(202, 201, 207, 1)
        }
    }
}
