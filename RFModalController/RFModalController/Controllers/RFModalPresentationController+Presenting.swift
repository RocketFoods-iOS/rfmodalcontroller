//
//  RFModalPresentationController+Presenting.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 17/04/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import AsyncDisplayKit

extension RFModalPresentationController {
    // swiftlint:disable function_body_length
    open override func presentationTransitionWillBegin() {
        super.presentationTransitionWillBegin()

        containerNode = containerNode ?? ASDisplayNode()

        guard let containerView = containerView,
            let containerNode = containerNode,
            let presentedView = presentedView,
            let window = containerView.window else { return }

        containerView.addSubnode(containerNode)

        if let indicator = indicator {
            indicatorView.color = indicator.tintColor

            let tap = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
            tap.cancelsTouchesInView = false

            indicatorView.addGestureRecognizer(tap)
            presentedView.addSubview(indicatorView)
        }

        updateLayoutIndicator()
        indicatorView.style = .arrow
        gradeView.alpha = 0

        let initialFrame = presentingViewController.isModal ? presentingViewController.view.frame : containerView.bounds

        containerView.insertSubview(snapshotViewContainer, belowSubview: presentedViewController.view)
        snapshotViewContainer.frame = initialFrame

        updateSnapshot()
        snapshotView?.layer.cornerRadius = 0

        backgroundView.backgroundColor = UIColor.black
        backgroundView.translatesAutoresizingMaskIntoConstraints = false

        containerView.insertSubview(backgroundView, belowSubview: snapshotViewContainer)

        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: window.topAnchor),
            backgroundView.leftAnchor.constraint(equalTo: window.leftAnchor),
            backgroundView.rightAnchor.constraint(equalTo: window.rightAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: window.bottomAnchor),
        ])

        let transformForSnapshotView = CGAffineTransform.identity
            .translatedBy(x: 0, y: -snapshotViewContainer.frame.origin.y)
            .translatedBy(x: 0, y: topSpace)
            .translatedBy(x: 0, y: -snapshotViewContainer.frame.height / 2)
            .scaledBy(x: scaleForPresentingView, y: scaleForPresentingView)
            .translatedBy(x: 0, y: snapshotViewContainer.frame.height / 2)

        addCornerRadiusAnimation(for: snapshotView, cornerRadius: cornerRadius, duration: 0.6)
        snapshotView?.layer.masksToBounds = true

        if #available(iOS 11.0, *) {
            presentedView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }

        presentedView.layer.cornerRadius = cornerRadius
        presentedView.layer.masksToBounds = true

        var rootSnapshotView: UIView?
        var rootSnapshotRoundedView: UIView?

        if presentingViewController.isModal {
            guard let rootController = presentingViewController.presentingViewController,
                let snapshotView = rootController.view.snapshotView(afterScreenUpdates: false) else { return }

            containerView.insertSubview(snapshotView, aboveSubview: backgroundView)
            snapshotView.frame = initialFrame
            snapshotView.transform = transformForSnapshotView
            snapshotView.alpha = 1 - alpha
            snapshotView.layer.cornerRadius = cornerRadius
            snapshotView.contentMode = .top
            snapshotView.layer.masksToBounds = true
            rootSnapshotView = snapshotView

            let snapshotRoundedView = UIView()
            snapshotRoundedView.layer.cornerRadius = cornerRadius
            snapshotRoundedView.layer.masksToBounds = true
            containerView.insertSubview(snapshotRoundedView, aboveSubview: snapshotView)
            snapshotRoundedView.frame = initialFrame
            snapshotRoundedView.transform = transformForSnapshotView
            rootSnapshotRoundedView = snapshotRoundedView
        }

        presentedViewController.transitionCoordinator?.animate(
            alongsideTransition: { [weak self] _ in

                self?.snapshotView?.transform = transformForSnapshotView
                self?.gradeView.alpha = self?.alpha ?? 1

            }, completion: { [weak self] _ in

                self?.snapshotView?.transform = .identity
                rootSnapshotView?.removeFromSuperview()
                rootSnapshotRoundedView?.removeFromSuperview()
            }
        )
    }

    open override func presentationTransitionDidEnd(_ completed: Bool) {
        super.presentationTransitionDidEnd(completed)

        guard let containerView = containerView else { return }

        updateSnapshot()
        presentedViewController.view.frame = frameOfPresentedViewInContainerView
        snapshotViewContainer.transform = .identity
        snapshotViewContainer.translatesAutoresizingMaskIntoConstraints = false
        snapshotViewContainer.centerXAnchor.constraint(equalTo: containerView.centerXAnchor).isActive = true
        updateSnapshotAspectRatio()

        if options?.contains(.tapOutside) ?? false {
            tap = UITapGestureRecognizer(target: self, action: #selector(dismissAction))
            tap?.cancelsTouchesInView = false

            guard let tap = tap else { return }

            snapshotViewContainer.addGestureRecognizer(tap)
        }

        if options?.contains(.swipe) ?? false {
            pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan))
            pan?.delegate = self
            pan?.maximumNumberOfTouches = 1
            pan?.cancelsTouchesInView = false

            guard let pan = pan else { return }

            presentedViewController.view.addGestureRecognizer(pan)
        }
    }
}
