//
//  RFModalOptions.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

public struct RFModalOptions: OptionSet {
    public let rawValue: Int

    public static let swipe = RFModalOptions(rawValue: 1 << 0)

    public static let tapOutside = RFModalOptions(rawValue: 1 << 1)

    public init(rawValue: Int) {
        self.rawValue = rawValue
    }
}
