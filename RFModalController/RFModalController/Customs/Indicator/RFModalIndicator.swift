//
//  RFModalIndicator.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

public struct RFModalIndicator {
    public var tintColor: UIColor

    public init(tintColor: UIColor) {
        self.tintColor = tintColor
    }
}

public class RFModalIndicatorView: UIView {
    public var style: Style = .line {
        didSet {
            switch style {
            case .line:
                animate {
                    self.leftView.transform = .identity
                    self.rightView.transform = .identity
                }

            case .arrow:
                animate {
                    let angle = CGFloat(20 * Float.pi / 180)

                    self.leftView.transform = CGAffineTransform(rotationAngle: angle)
                    self.rightView.transform = CGAffineTransform(rotationAngle: -angle)
                }
            }
        }
    }

    public var color: UIColor = UIColor(202, 201, 207, 1) {
        didSet {
            leftView.backgroundColor = color
            rightView.backgroundColor = color
        }
    }

    private var leftView: UIView = UIView()
    private var rightView: UIView = UIView()

    init() {
        super.init(frame: .zero)

        backgroundColor = UIColor.clear

        addSubview(leftView)
        addSubview(rightView)

        color = UIColor(202, 201, 207, 1)
    }

    public required init?(coder _: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override public func sizeToFit() {
        super.sizeToFit()

        frame = CGRect(x: frame.origin.x, y: frame.origin.y, width: 36, height: 13)

        let height: CGFloat = 5
        let correction = height / 2

        leftView.frame = CGRect(x: 0, y: 0, width: frame.width / 2 + correction, height: height)
        leftView.center.y = frame.height / 2
        leftView.layer.cornerRadius = min(leftView.frame.width, leftView.frame.height) / 2

        rightView.frame = CGRect(
            x: frame.width / 2 - correction,
            y: 0,
            width: frame.width / 2 + correction,
            height: height
        )

        rightView.center.y = frame.height / 2
        rightView.layer.cornerRadius = min(leftView.frame.width, leftView.frame.height) / 2
    }

    private func animate(animations: @escaping (() -> Void)) {
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 1,
            options: [.beginFromCurrentState, .curveEaseOut],
            animations: animations
        )
    }

    public enum Style {
        case arrow
        case line
    }
}
