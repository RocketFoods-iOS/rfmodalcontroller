//
//  RFModalIndicatorNode.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 12/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import AsyncDisplayKit
import RFUI

public class RFModalIndicatorNode: ASDisplayNode {
    public enum Shape {
        case arrow
        case line
    }

    public var shape: Shape = .line {
        didSet {
            switch shape {
            case .line:
                self.animate {
                    self.leftNode.transform = CATransform3DIdentity
                    self.rightNode.transform = CATransform3DIdentity
                }

            case .arrow:
                self.animate {
                    let angle = CGFloat(20 * Float.pi / 180)

                    self.leftNode.transform = CATransform3DRotate(self.leftNode.transform, angle, 0, 0, 0)
                    self.rightNode.transform = CATransform3DRotate(self.rightNode.transform, -angle, 0, 0, 0)
                }
            }
        }
    }

    override public var tintColor: UIColor! {
        didSet {
            leftNode.backgroundColor = tintColor
            rightNode.backgroundColor = tintColor
        }
    }

    private var leftNode: ASDisplayNode = ASDisplayNode()
    private var rightNode: ASDisplayNode = ASDisplayNode()

    override init() {
        super.init()

        backgroundColor = .clear
        tintColor = UIColor(202, 201, 207, 1)

        style.preferredSize = CGSize(width: 36, height: 13)

        automaticallyManagesSubnodes = true
    }

    required public init?(coder _: NSCoder) { fatalError("init(coder:) has not been implemented") }

    override public func layoutSpecThatFits(_: ASSizeRange) -> ASLayoutSpec {
        let horizontalStack = RFHorizontalStack()

        horizontalStack.horizontalAlignment = .middle
        horizontalStack.verticalAlignment = .center

        let height = CGFloat(5)
        let correction = height / 2

        leftNode.style.preferredSize = CGSize(width: frame.width / 2 + correction, height: height)
        leftNode.cornerRadius = min(leftNode.style.preferredSize.width, leftNode.style.preferredSize.height) / 2

        horizontalStack.addElement(leftNode)

        rightNode.style.preferredSize = CGSize(width: frame.width / 2 + correction, height: height)
        rightNode.cornerRadius = min(leftNode.style.preferredSize.width, leftNode.style.preferredSize.height) / 2

        horizontalStack.addElement(rightNode)

        return horizontalStack
    }

    private func animate(animations: @escaping (() -> Void)) {
        UIView.animate(
            withDuration: 0.5,
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 1,
            options: [.beginFromCurrentState, .curveEaseOut],
            animations: animations
        )
    }
}
