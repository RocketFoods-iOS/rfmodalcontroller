//
//  UIView.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import Foundation

public extension UIView {
    public var controller: UIViewController? {
        guard let controller = self.next as? UIViewController else {
            guard let nextView = self.next as? UIView else { return nil }

            return nextView.controller
        }

        return controller
    }
}
