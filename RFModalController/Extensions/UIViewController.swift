//
//  UIViewController.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

extension UIViewController {
    var isModal: Bool {
        return presentingViewController != nil &&
            modalPresentationStyle == .custom &&
            transitioningDelegate is RFModalTransitioningDelegate
    }

    public func presentModal(_ controller: UIViewController, height: CGFloat? = nil,
                      showIndicator: Bool = false, complection: (() -> Void)? = nil) {
        let transitionDelegate = RFModalTransitioningDelegate()

        transitionDelegate.height = height

        let indicator = showIndicator ? RFModalIndicator(tintColor: .mainTintLight) : nil

        transitionDelegate.indicator = indicator

        controller.transitioningDelegate = transitionDelegate

        controller.modalPresentationStyle = .custom
        controller.modalPresentationCapturesStatusBarAppearance = true

        present(controller, animated: true, completion: complection)
    }
}
