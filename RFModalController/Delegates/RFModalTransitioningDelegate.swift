//
//  RFModalTransitioningDelegate.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

public final class RFModalTransitioningDelegate: NSObject, UIViewControllerTransitioningDelegate {
    public var options: RFModalOptions?
    public var dismissTranslation: CGFloat

    public var indicator: RFModalIndicator?

    public var height: CGFloat?
    public var cornerRadius: CGFloat

    public override init() {
        options = nil
        dismissTranslation = 200
        indicator = nil
        height = nil
        cornerRadius = 10

        super.init()
    }

    public init(with options: RFModalOptions? = nil, translation: CGFloat = 200,
                indicator: RFModalIndicator? = nil, height: CGFloat? = nil, cornerRadius: CGFloat = 10) {
        self.options = options
        dismissTranslation = translation
        self.indicator = indicator
        self.height = height
        self.cornerRadius = cornerRadius

        super.init()
    }

    public func presentationController(forPresented presented: UIViewController,
                                       presenting: UIViewController?,
                                       source _: UIViewController) -> UIPresentationController? {
        let controller = RFModalPresentationController(presentedViewController: presented, presenting: presenting)

        controller.options = options
        controller.indicator = indicator
        controller.height = height
        controller.dismissTranslation = dismissTranslation
        controller.cornerRadius = cornerRadius
        controller.transitioningDelegate = self

        return controller
    }

    public func animationController(forPresented _: UIViewController, presenting _: UIViewController,
                                    source _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return RFModalPresentingAnimationController()
    }

    public func animationController(forDismissed _: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        return RFModalDismissingAnimationController()
    }
}
