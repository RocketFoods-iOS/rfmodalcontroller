//
//  RFModalPresentationController+Dismissing.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 17/04/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import AsyncDisplayKit

extension RFModalPresentationController {
    @objc public func dismissAction() {
        presentingViewController.view.endEditing(true)
        presentedViewController.view.endEditing(true)
        presentedViewController.dismiss(animated: true, completion: nil)
    }

    // swiftlint:disable function_body_length
    open override func dismissalTransitionWillBegin() {
        super.dismissalTransitionWillBegin()

        guard let containerView = containerView else { return }
        startDismissing = true

        let initialFrame: CGRect =
            presentingViewController.isModal ? presentingViewController.view.frame : containerView.bounds

        let initialTransform = CGAffineTransform.identity
            .translatedBy(x: 0, y: -initialFrame.origin.y)
            .translatedBy(x: 0, y: topSpace)
            .translatedBy(x: 0, y: -initialFrame.height / 2)
            .scaledBy(x: scaleForPresentingView, y: scaleForPresentingView)
            .translatedBy(x: 0, y: initialFrame.height / 2)

        snapshotViewTopConstraint?.isActive = false
        snapshotViewWidthConstraint?.isActive = false
        snapshotViewAspectRatioConstraint?.isActive = false
        snapshotViewContainer.translatesAutoresizingMaskIntoConstraints = true
        snapshotViewContainer.frame = initialFrame
        snapshotViewContainer.transform = initialTransform

        let finalCornerRadius = presentingViewController.isModal ? cornerRadius : 0
        let finalTransform: CGAffineTransform = .identity

        addCornerRadiusAnimation(for: snapshotView, cornerRadius: finalCornerRadius, duration: 0.6)

        var rootSnapshotView: UIView?
        var rootSnapshotRoundedView: UIView?

        if presentingViewController.isModal {
            guard let rootController = presentingViewController.presentingViewController,
                let snapshotView = rootController.view.snapshotView(afterScreenUpdates: false) else { return }

            containerView.insertSubview(snapshotView, aboveSubview: backgroundView)
            snapshotView.frame = initialFrame
            snapshotView.transform = initialTransform
            snapshotView.contentMode = .top
            rootSnapshotView = snapshotView
            snapshotView.layer.cornerRadius = cornerRadius
            snapshotView.layer.masksToBounds = true

            let snapshotRoundedView = UIView()
            snapshotRoundedView.layer.cornerRadius = cornerRadius
            snapshotRoundedView.layer.masksToBounds = true
            snapshotRoundedView.backgroundColor = UIColor.black.withAlphaComponent(alpha)
            containerView.insertSubview(snapshotRoundedView, aboveSubview: snapshotView)
            snapshotRoundedView.frame = initialFrame
            snapshotRoundedView.transform = initialTransform
            rootSnapshotRoundedView = snapshotRoundedView
        }

        presentedViewController.transitionCoordinator?.animate(
            alongsideTransition: { [weak self] _ in

                guard let self = self else { return }

                self.snapshotView?.transform = .identity
                self.snapshotViewContainer.transform = finalTransform
                self.gradeView.alpha = 0

            }, completion: { _ in

                rootSnapshotView?.removeFromSuperview()
                rootSnapshotRoundedView?.removeFromSuperview()
            }
        )
    }

    open override func dismissalTransitionDidEnd(_ completed: Bool) {
        super.dismissalTransitionDidEnd(completed)

        guard let containerView = containerView else { return }

        backgroundView.removeFromSuperview()
        snapshotView?.removeFromSuperview()
        snapshotViewContainer.removeFromSuperview()
        indicatorView.removeFromSuperview()

        let offscreenFrame = CGRect(
            x: 0,
            y: containerView.bounds.height,
            width: containerView.bounds.width,
            height: containerView.bounds.height
        )

        presentedViewController.view.frame = offscreenFrame
        presentedViewController.view.transform = .identity
    }
}
