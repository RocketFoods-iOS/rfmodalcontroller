//
//  RFModalController.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import AsyncDisplayKit

open class RFModalController {
    public static var topScrollIndicatorInset: CGFloat { 6 }

    public static func scrollViewDidScroll(_ scrollView: UIScrollView) {
        guard let controller = scrollView.controller else { return }

        guard let presentationController =
            controller.presentationController as? RFModalPresentationController else { return }

        let translation = -(scrollView.contentOffset.y + scrollView.contentInset.top)

        guard translation >= 0 else {
            presentationController.setIndicator(style: .arrow)
            presentationController.scrollViewDidScroll(0)

            return
        }

        guard !controller.isBeingPresented else { return }

        scrollView.subviews.forEach { $0.transform = CGAffineTransform(translationX: 0, y: -translation) }

        presentationController.setIndicator(style: scrollView.isTracking ? .line : .arrow)

        let dismissTranslation = presentationController.dismissTranslation ?? RFModalConstants.Node.dismissTranslation

        guard translation < dismissTranslation * 0.4, scrollView.isTracking || scrollView.isDragging else {
            return presentationController.presentedViewController.dismiss(animated: true, completion: nil)
        }

        guard presentationController.pan?.state != UIGestureRecognizer.State.changed else {
            presentationController.setIndicator(style: .arrow)
            presentationController.scrollViewDidScroll(0)

            return
        }

        presentationController.scrollViewDidScroll(translation * 2)

//    if translation >= 0 {
//      if controller.isBeingPresented { return }
//      scrollView.subviews.forEach {
//        $0.transform = CGAffineTransform(translationX: 0, y: -translation)
//      }
//      presentationController.setIndicator(style: scrollView.isTracking ? .line : .arrow)
//      if translation >= presentationController.dismissTranslation ?? 200 * 0.4 {
//        if !scrollView.isTracking && !scrollView.isDragging {
//          presentationController.presentedViewController.dismiss(animated: true, completion: nil)
//          return
//        }
//      }
//      if presentationController.pan?.state != UIGestureRecognizer.State.changed {
//        presentationController.scrollViewDidScroll(translation * 2)
//      }
//    } else {
//      presentationController.setIndicator(style: .arrow)
//      presentationController.scrollViewDidScroll(0)
//    }
    }

    public static func updatePresentingController(parent controller: UIViewController) {
        guard let presentationController =
            controller.presentedViewController?.presentationController as? RFModalPresentationController else { return }

        presentationController.updatePresentingController()

        if let presentationController =
            controller.presentedViewController?.presentationController as? RFModalPresentationController {
            presentationController.updatePresentingController()
        }
    }

    public static func updatePresentingController(modal controller: UIViewController) {
        guard let presentationController =
            controller.presentationController as? RFModalPresentationController else { return }

        presentationController.updatePresentingController()

//    if let presentationController = controller.presentationController as? RFModalPresentationController {
//      presentationController.updatePresentingController()
//    }
    }
}
