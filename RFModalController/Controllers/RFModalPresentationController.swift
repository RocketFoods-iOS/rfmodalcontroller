//
//  RFModalPresentationController.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

import AsyncDisplayKit

open class RFModalPresentationController: UIPresentationController, UIGestureRecognizerDelegate {
    open var options: RFModalOptions?
    open var dismissTranslation: CGFloat?

    open var indicator: RFModalIndicator?

    open var height: CGFloat?
    
    open var transitionDuration = TimeInterval(0.1)

    open weak var transitioningDelegate: RFModalTransitioningDelegate?

    public var pan: UIPanGestureRecognizer?
    public var tap: UITapGestureRecognizer?

    public var indicatorView = RFModalIndicatorView()
    public var gradeView: UIView = UIView()
    public let snapshotViewContainer = UIView()
    public var snapshotView: UIView?
    public let backgroundView = UIView()

    public var containerNode: ASDisplayNode?

    public var snapshotViewTopConstraint: NSLayoutConstraint?
    public var snapshotViewWidthConstraint: NSLayoutConstraint?
    public var snapshotViewAspectRatioConstraint: NSLayoutConstraint?

    public var workGester: Bool = false
    public var startDismissing: Bool = false

    public var topSpace: CGFloat {
        let statusBarHeight: CGFloat = UIApplication.shared.statusBarFrame.height

        return statusBarHeight < 25 ? 30 : statusBarHeight
    }

    public let alpha = CGFloat(0.51)
    public var cornerRadius = CGFloat(10)

    public var scaleForPresentingView: CGFloat {
        guard let containerView = containerView else { return 0 }

        let factor = 1 - (topSpace * 2 / containerView.frame.height)

        return factor
    }

    open override var frameOfPresentedViewInContainerView: CGRect {
        guard let containerView = containerView else { return .zero }

        var height = self.height ?? containerView.bounds.height

        height = height > containerView.bounds.height ? containerView.bounds.height : height

        let additionTranslate = containerView.bounds.height - height
        let yOffset: CGFloat = topSpace + 13 + additionTranslate

        return CGRect(x: 0, y: yOffset, width: containerView.bounds.width, height: containerView.bounds.height - yOffset)
    }
}

extension RFModalPresentationController {
    @objc public func handlePan(gestureRecognizer: UIPanGestureRecognizer) {
        guard gestureRecognizer.isEqual(pan), options?.contains(.swipe) ?? false else { return }

        if gestureRecognizer.state == .began {
            workGester = true
            indicatorView.style = .line

            presentingViewController.view.layer.removeAllAnimations()
            presentingViewController.view.endEditing(true)
            presentedViewController.view.endEditing(true)

            gestureRecognizer.setTranslation(CGPoint(x: 0, y: 0), in: containerView)

        } else if gestureRecognizer.state == .changed {
            workGester = true

            if options?.contains(.swipe) ?? false {
                let translation = gestureRecognizer.translation(in: presentedView)

                updatePresentedViewForTranslation(inVerticalDirection: translation.y)

            } else {
                gestureRecognizer.setTranslation(.zero, in: presentedView)
            }

        } else if gestureRecognizer.state == .ended {
            workGester = false

            let translation = gestureRecognizer.translation(in: presentedView).y

            if translation >= dismissTranslation ?? 0 {
                presentedViewController.dismiss(animated: true, completion: nil)

            } else {
                indicatorView.style = .arrow

                UIView.animate(
                    withDuration: 0.6,
                    delay: 0,
                    usingSpringWithDamping: 1,
                    initialSpringVelocity: 1,
                    options: [.curveEaseOut, .allowUserInteraction],
                    animations: { [weak self] in

                        self?.snapshotView?.transform = .identity
                        self?.presentedView?.transform = .identity

                        self?.gradeView.alpha = self?.alpha ?? 1
                    }
                )
            }
        }
    }

    public func scrollViewDidScroll(_ translation: CGFloat) {
        if !workGester {
            updatePresentedViewForTranslation(inVerticalDirection: translation)
        }
    }

    public func updatePresentingController() {
        if startDismissing { return }

        updateSnapshot()
    }

    public func setIndicator(style: RFModalIndicatorView.Style) {
        indicatorView.style = style
    }

    public func updatePresentedViewForTranslation(inVerticalDirection translation: CGFloat) {
//        if startDismissing { return }
//
//        let elasticThreshold: CGFloat = 120
//        let translationFactor: CGFloat = 1 / 2
//
//        if translation >= 0 {
//            let translationForModal: CGFloat = {
//                if translation >= elasticThreshold {
//                    let frictionLength = translation - elasticThreshold
//                    let frictionTranslation = 30 * atan(frictionLength / 120) + frictionLength / 10
//
//                    return frictionTranslation + (elasticThreshold * translationFactor)
//
//                } else { return translation * translationFactor }
//
//            }()
//
//            presentedView?.transform = CGAffineTransform(translationX: 0, y: translationForModal)
//
//            let scaleFactor = 1 + (translationForModal / 5000)
//            snapshotView?.transform = CGAffineTransform(scaleX: scaleFactor, y: scaleFactor)
//            let gradeFactor = 1 + (translationForModal / 7000)
//            gradeView.alpha = alpha - ((gradeFactor - 1) * 15)
//
//        } else {
//            presentedView?.transform = CGAffineTransform.identity
//        }
    }
}

extension RFModalPresentationController {
    open override func containerViewWillLayoutSubviews() {
        super.containerViewWillLayoutSubviews()

        guard let containerView = containerView else { return }

        updateSnapshotAspectRatio()

        if presentedViewController.view.isDescendant(of: containerView) {
            UIView.animate(withDuration: transitionDuration) { [weak self] in

                guard let frameOfPresentedViewInContainerView = self?.frameOfPresentedViewInContainerView else { return }

                self?.presentedViewController.view.frame = frameOfPresentedViewInContainerView
            }
        }
    }

    open override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        super.viewWillTransition(to: size, with: coordinator)
        
        height = size.height

        coordinator.animate(alongsideTransition: { [weak self] _ in

            self?.updateLayoutIndicator()

        }, completion: { [weak self] _ in

            self?.updateSnapshotAspectRatio()
            self?.updateSnapshot()

        })
    }

    public func updateLayoutIndicator() {
        guard let presentedView = presentedView else { return }

        indicatorView.style = .line
        indicatorView.sizeToFit()
        indicatorView.frame.origin.y = 12
        indicatorView.center.x = presentedView.frame.width / 2
    }

    public func updateSnapshot() {
        guard let currentSnapshotView = presentingViewController.view.snapshotView(afterScreenUpdates: true) else { return }

        snapshotView?.removeFromSuperview()
        snapshotViewContainer.addSubview(currentSnapshotView)
        constraints(view: currentSnapshotView, to: snapshotViewContainer)
        snapshotView = currentSnapshotView
        snapshotView?.layer.cornerRadius = cornerRadius
        snapshotView?.layer.masksToBounds = true

        if #available(iOS 11.0, *) {
            snapshotView?.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        }

        gradeView.removeFromSuperview()
        gradeView.backgroundColor = UIColor.black

        guard let snapshotView = snapshotView else { return }

        snapshotView.addSubview(gradeView)
        constraints(view: gradeView, to: snapshotView)
    }

    public func updateSnapshotAspectRatio() {
        guard let containerView = containerView,
            snapshotViewContainer.translatesAutoresizingMaskIntoConstraints == false else { return }

        snapshotViewTopConstraint?.isActive = false
        snapshotViewWidthConstraint?.isActive = false
        snapshotViewAspectRatioConstraint?.isActive = false

        let snapshotReferenceSize = presentingViewController.view.frame.size
        let aspectRatio = snapshotReferenceSize.width / snapshotReferenceSize.height

        snapshotViewTopConstraint = snapshotViewContainer.topAnchor.constraint(
            equalTo: containerView.topAnchor,
            constant: topSpace
        )

        snapshotViewWidthConstraint = snapshotViewContainer.widthAnchor.constraint(
            equalTo: containerView.widthAnchor,
            multiplier: scaleForPresentingView
        )

        snapshotViewAspectRatioConstraint = snapshotViewContainer.widthAnchor.constraint(
            equalTo: snapshotViewContainer.heightAnchor,
            multiplier: aspectRatio
        )

        snapshotViewTopConstraint?.isActive = true
        snapshotViewWidthConstraint?.isActive = true
        snapshotViewAspectRatioConstraint?.isActive = true
    }

    public func constraints(view: UIView, to superView: UIView) {
        view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            view.topAnchor.constraint(equalTo: superView.topAnchor),
            view.leftAnchor.constraint(equalTo: superView.leftAnchor),
            view.rightAnchor.constraint(equalTo: superView.rightAnchor),
            view.bottomAnchor.constraint(equalTo: superView.bottomAnchor),
        ])
    }

    public func addCornerRadiusAnimation(for view: UIView?, cornerRadius: CGFloat, duration: CFTimeInterval) {
        guard let view = view else { return }

        let animation = CABasicAnimation(keyPath: "cornerRadius")

        animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
        animation.fromValue = view.layer.cornerRadius
        animation.toValue = cornerRadius
        animation.duration = duration

        view.layer.add(animation, forKey: "cornerRadius")
        view.layer.cornerRadius = cornerRadius
    }
}
