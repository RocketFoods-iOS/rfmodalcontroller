//
//  RFModalPresentingAnimationController.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

public final class RFModalPresentingAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let presentedController = transitionContext.viewController(forKey: .to) else { return }

        let containerView = transitionContext.containerView
        containerView.addSubview(presentedController.view)

        let finalPresentedViewFrame = transitionContext.finalFrame(for: presentedController)
        presentedController.view.frame = finalPresentedViewFrame
        presentedController.view.frame.origin.y = containerView.bounds.height

        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 1,
            options: .curveEaseOut,
            animations: { presentedController.view.frame = finalPresentedViewFrame },
            completion: { transitionContext.completeTransition($0) }
        )
    }

    public func transitionDuration(using _: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.6
    }
}
