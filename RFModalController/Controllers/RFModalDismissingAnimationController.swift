//
//  RFModalDismissingAnimationController.swift
//  RFModalController
//
//  Created by Nikita Arutyunov on 11/03/2019.
//  Copyright © 2019 Nikita Arutyunov. All rights reserved.
//

public final class RFModalDismissingAnimationController: NSObject, UIViewControllerAnimatedTransitioning {
    public func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard let presentedViewController = transitionContext.viewController(forKey: .from) else { return }

        let containerView = transitionContext.containerView

        let offscreenFrame = CGRect(
            x: 0,
            y: containerView.bounds.height,
            width: containerView.bounds.width,
            height: containerView.bounds.height
        )

        UIView.animate(
            withDuration: transitionDuration(using: transitionContext),
            delay: 0,
            usingSpringWithDamping: 1,
            initialSpringVelocity: 1,
            options: .curveEaseIn,
            animations: { presentedViewController.view.frame = offscreenFrame },
            completion: { transitionContext.completeTransition($0) }
        )
    }

    public func transitionDuration(using _: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.6
    }
}
